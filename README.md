# Packager - Android App For Android Development Finals

![icon](https://bitbucket.org/VazhaKentchiashvili/packager/raw/29d4c135e1e35d708aa83957eb758e8770e35f22/Readme/icon_packager.png)

Packager is a parcel sending app.

## About

Packager has been made for you to comfortably send and receive parcels.
You can easily sign up in the app and use the app freely, without any constraints.

This android application allows you to send and receive parcels from around the globe, where our service is available(Currently limited amount of post offices).

This application was originally created by Tabagari Mariami and Vazha kentchiashvili and as a Final Project for the SDASU Android Development course.


## Features

The android app lets you:

- Send parcels of different sizes from your current location, to available post offices in different countries.
- See your outgoing and incoming parcels.
- Completely ad-free.
- Add your frequent parcel receivers to your favorites list.
- Look for your outgoing parcels through the receiver's personal number.
- Track your outgoing parcels.


## Technologies Used

- Dagger Hilt.
- Retrofit2.
- Firebase SDK: Firebase Authentication, FirebaseFirestore.
- AndroidX LifeCycle: Viewmodel, LiveData.
- Coroutines
- Glide
- Lottie Animations
- ViewPager2
- Google Map SDK
- DataStore

The "Device Location" permission is used when the user navigates to the Send Parcel Fragment.