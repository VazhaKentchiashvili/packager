package com.packager.packager.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.packager.packager.screens.bottom_nav_screens.home.ParcelsUseCase
import com.packager.packager.screens.bottom_nav_screens.sendParcel.CheckInfoUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UseCasesModule {

    @Provides
    @Singleton
    fun parcelInfoCheckUseCase(): CheckInfoUseCase = CheckInfoUseCase()

    @Provides
    @Singleton
    fun parcelsUseCase(
        firebaseAuth: FirebaseAuth,
        firebaseFirestore: FirebaseFirestore
    ): ParcelsUseCase = ParcelsUseCase(firebaseFirestore,firebaseAuth)
}