package com.packager.packager.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.packager.packager.BuildConfig
import com.packager.packager.repositoy.manage_password_repository.ManagePasswordRepoImplement
import com.packager.packager.repositoy.signUpRepository.SignUpRepoImplement
import com.packager.packager.repositoy.uploadParcelRepository.UploadParcelRepoImplement
import com.packager.packager.retrofit.ConditionsApiService
import com.packager.packager.retrofit.NewsApiService
import com.packager.packager.retrofit.ParcelLocationApiService
import com.packager.packager.user_pref.UserPreferences
import com.packager.packager.utils.Constants.CONDITIONS_BASE_URL
import com.packager.packager.utils.Constants.NEWS_BASE_URL
import com.packager.packager.utils.Constants.PARCEL_LOCATION_BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    private fun interceptorClient(): OkHttpClient {
        val builder = OkHttpClient.Builder().addInterceptor(Interceptor { chain ->
            chain.request().url
            val request = chain.request().newBuilder()
            val response = chain.proceed(request.build())
            response
        })

        if (BuildConfig.BUILD_TYPE == "debug") {
            builder.addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }
            )
        }
        return builder.build()
    }

    @Provides
    @Singleton
    fun newsApiService(): NewsApiService =
        Retrofit.Builder()
            .baseUrl(NEWS_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(interceptorClient())
            .build()
            .create(NewsApiService::class.java)

    @Provides
    @Singleton
    fun parcelLocationService(): ParcelLocationApiService =
        Retrofit.Builder()
            .baseUrl(PARCEL_LOCATION_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(interceptorClient())
            .build()
            .create(ParcelLocationApiService::class.java)

    @Provides
    @Singleton
    fun conditionsService(): ConditionsApiService =
        Retrofit.Builder()
            .baseUrl(CONDITIONS_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(interceptorClient())
            .build()
            .create(ConditionsApiService::class.java)

    @Singleton
    @Provides
    fun signupRepoImpl(): SignUpRepoImplement = SignUpRepoImplement()

    // Firebase Provides

    @Singleton
    @Provides
    fun managePasswordRepoImplement(): ManagePasswordRepoImplement = ManagePasswordRepoImplement()

    @Singleton
    @Provides
    fun uploadParcelRepoImplement(): UploadParcelRepoImplement = UploadParcelRepoImplement()

    @Provides
    @Singleton
    fun firebaseAuth(): FirebaseAuth = FirebaseAuth.getInstance()

    @Provides
    @Singleton
    fun firebaseFirestore(): FirebaseFirestore = FirebaseFirestore.getInstance()
}