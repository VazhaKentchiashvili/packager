package com.packager.packager.models

data class FavoritesModel(
    val name:String,
    val lastName:String,
    val email:String,
    val privateNumber:String,
    val mobileNumber:String,
    val uid:String
)
