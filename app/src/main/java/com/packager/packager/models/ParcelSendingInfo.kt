package com.packager.packager.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ParcelSendingInfo(
    val sendFrom: String? = null,
    val sendWhere: String? = null,
    val sendServiceCenter: String? = null,
    val addresseeFirstName: String? = null,
    val addresseeLastName: String? = null,
    val addresseePersonalNumber: String? = null,
    val addresseePhoneNumber: String? = null,
    val addresseeEmail:String? = null,
    val parcelWeight: String? = null,
    val parcelValue: String? = null,
    val parcelVolume: String? = null,
    val parcelTransportTime: String? = null,
    val isConditionsAgreed: Boolean? = false,
): Parcelable
