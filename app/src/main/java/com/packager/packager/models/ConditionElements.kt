package com.packager.packager.models

data class ConditionElements(
    val condition_description: String,
    val condition_number: String
)