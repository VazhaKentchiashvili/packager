package com.packager.packager.models

data class UserModel(
    val name:String,
    val lastName:String,
    val email:String,
    val privateNumber:String,
    val mobileNumber:String,
)