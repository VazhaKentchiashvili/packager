package com.packager.packager.models

import com.google.gson.annotations.SerializedName

data class AllServiceCenters(
    @SerializedName("send_parcel_from")
    val sendParcelFrom: List<String>,
    @SerializedName("send_parcel_where")
    val sendParcelWhere: List<String>,
    @SerializedName("service_center_gr")
    val serviceCenterGr: List<String>,
    @SerializedName("service_center_il")
    val serviceCenterIl: List<String>,
    @SerializedName("service_center_it")
    val serviceCenterIt: List<String>,
    @SerializedName("service_center_tr")
    val serviceCenterTr: List<String>,
    @SerializedName("service_center_usa")
    val serviceCenterUsa: List<String>
)