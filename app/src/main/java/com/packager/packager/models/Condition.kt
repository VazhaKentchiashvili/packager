package com.packager.packager.models

data class Condition(
    val conditions: List<ConditionElements>
)