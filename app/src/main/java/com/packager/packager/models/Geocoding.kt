package com.packager.packager.models

data class Geocoding(
    val results: List<Result>?,
    val status: String?,
) {

    data class Result(
        val formatted_address: String?,
        val geometry: Geometry?,
        val address_components: List<AddressComponent>?,
    )

    data class Geometry(
        val location: Location?,
    )

    data class Location(
        val lat: Double?,
        val lng: Double?
    )

    data class AddressComponent(
        val long_name: String?,
        val short_name: String?,
    )
}