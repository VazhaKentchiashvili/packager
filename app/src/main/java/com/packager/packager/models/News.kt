package com.packager.packager.models

data class News(
    val news: List<NewsItems>
)
