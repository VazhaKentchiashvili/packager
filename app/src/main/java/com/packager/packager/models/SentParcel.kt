package com.packager.packager.models

data class SentParcel(
    val sentFrom: String,
    val sentWhere: String,
)