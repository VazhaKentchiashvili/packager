package com.packager.packager.models

data class NewsItems(
    val image: String,
    val title: String,
)
