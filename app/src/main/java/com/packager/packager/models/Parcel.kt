package com.packager.packager.models

data class Parcel(

    val parcelSender: String,
    val parcelReceiver:String,
    val sendFrom:String,
    val sendWhere:String,
    val email:String
    )