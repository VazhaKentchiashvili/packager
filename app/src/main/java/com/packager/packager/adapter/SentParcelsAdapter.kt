package com.packager.packager.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.packager.packager.databinding.SentParcelCardViewBinding
import com.packager.packager.models.Parcel

class SentParcelsAdapter : RecyclerView.Adapter<SentParcelsAdapter.SentParcelsViewHolder>() {

    private val sentParcelList = mutableListOf<Parcel?>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SentParcelsViewHolder(
            SentParcelCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: SentParcelsViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = sentParcelList.size

    inner class SentParcelsViewHolder(private val binding: SentParcelCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind() {
            val currentSentParcel: Parcel? = sentParcelList[absoluteAdapterPosition]
            binding.tvParcelTitle.text = currentSentParcel?.parcelReceiver
            binding.tvSendParcelFrom.text = currentSentParcel?.sendFrom
            binding.tvSendParcelWhere.text = currentSentParcel?.sendWhere
        }
    }

    fun setData(sentParcel: List<Parcel>) {
        sentParcelList.clear()
        sentParcelList.addAll(sentParcel)
        notifyDataSetChanged()
    }
}