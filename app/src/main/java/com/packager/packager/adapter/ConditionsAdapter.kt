package com.packager.packager.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.packager.packager.databinding.ConditionsItemCardViewBinding
import com.packager.packager.models.ConditionElements

class ConditionsAdapter : RecyclerView.Adapter<ConditionsAdapter.ConditionsViewHolder>() {

    private val conditionsList = mutableListOf<ConditionElements>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ConditionsViewHolder(
            ConditionsItemCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ConditionsViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = conditionsList.size

    inner class ConditionsViewHolder(private val binding: ConditionsItemCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var currentCondition: ConditionElements

        fun bind() {
            currentCondition = conditionsList[absoluteAdapterPosition]

            binding.conditionNumber.text =
                currentCondition.condition_number
            binding.conditionDescription.text =
                currentCondition.condition_description
        }
    }

    fun setData(conditions: MutableList<ConditionElements>) {
        conditionsList.clear()
        conditionsList.addAll(conditions)
        notifyDataSetChanged()
    }
}