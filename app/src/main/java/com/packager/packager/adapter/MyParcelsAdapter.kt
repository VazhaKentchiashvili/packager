package com.packager.packager.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.packager.packager.databinding.SentParcelCardViewBinding
import com.packager.packager.models.Parcel

class MyParcelsAdapter : RecyclerView.Adapter<MyParcelsAdapter.ReceivedParcelsViewHolder>() {

    private val receivedParcelList = mutableListOf<Parcel?>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ReceivedParcelsViewHolder(
            SentParcelCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ReceivedParcelsViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = receivedParcelList.size

    inner class ReceivedParcelsViewHolder(private val binding: SentParcelCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind() {
            val currentReceivedParcel: Parcel? = receivedParcelList[absoluteAdapterPosition]
            binding.tvParcelTitle.text = currentReceivedParcel?.parcelReceiver
            binding.tvSendParcelFrom.text = currentReceivedParcel?.sendFrom
            binding.tvSendParcelWhere.text = currentReceivedParcel?.sendWhere
        }
    }

    fun setData(receivedParcel: List<Parcel>) {
        receivedParcelList.clear()
        receivedParcelList.addAll(receivedParcel)
        notifyDataSetChanged()
    }
}