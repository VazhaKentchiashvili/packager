package com.packager.packager.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.packager.packager.databinding.ItemRecyclerFavoritesBinding
import com.packager.packager.databinding.SentParcelCardViewBinding
import com.packager.packager.models.FavoritesModel
import com.packager.packager.models.Parcel

typealias itemClick = (position: Int, data: FavoritesModel) -> Unit

class FavoritesAdapter : RecyclerView.Adapter<FavoritesAdapter.FavoritesViewHolder>() {

    private val favoritesList = mutableListOf<FavoritesModel?>()
    lateinit var itemClick: itemClick

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        FavoritesViewHolder(
            ItemRecyclerFavoritesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: FavoritesViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = favoritesList.size

    inner class FavoritesViewHolder(private val binding: ItemRecyclerFavoritesBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        lateinit var favoriteInfo: FavoritesModel

        fun bind() {
            favoriteInfo = favoritesList[absoluteAdapterPosition]!!
            binding.tvFavName.text = favoriteInfo.name
            binding.tvFavNumber.text = favoriteInfo.mobileNumber

            binding.btnSendFavoriteParcel.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            itemClick(absoluteAdapterPosition, favoriteInfo)
        }
    }

    fun setData(favorites: List<FavoritesModel>) {
        favoritesList.clear()
        favoritesList.addAll(favorites)
        notifyDataSetChanged()
    }
}