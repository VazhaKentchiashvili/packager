package com.packager.packager.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.packager.packager.databinding.NewsCardViewBinding
import com.packager.packager.extensions.setImageWithGlide
import com.packager.packager.models.News
import com.packager.packager.models.NewsItems

class NewsSliderAdapter : RecyclerView.Adapter<NewsSliderAdapter.NewsSliderViewHolder>() {

    private val newsList = mutableListOf<NewsItems>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsSliderViewHolder {
        return NewsSliderViewHolder(
            NewsCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: NewsSliderViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = newsList.size

    inner class NewsSliderViewHolder(private val binding: NewsCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var currentNews: NewsItems

        fun bind() {
            currentNews = newsList[absoluteAdapterPosition]

            binding.imNews.setImageWithGlide(currentNews.image)
            binding.tvNewsTitle.text = currentNews.title
        }
    }

    fun setData(addedNews: MutableList<NewsItems>) {
        newsList.clear()
        newsList.addAll(addedNews)
        notifyDataSetChanged()
    }

}