package com.packager.packager.extensions

import android.content.Context
import android.util.Log.d
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import com.google.firebase.firestore.FirebaseFirestore

fun FirebaseFirestore.getDropdownItems(
    view: AutoCompleteTextView,
    context: Context,
    layoutId: Int,
    collectionPath: String,
    documentPath: String,
) {
    collection(collectionPath)
        .document(documentPath)
        .get()
        .addOnSuccessListener { documents ->
            if (documents != null) {
                view.setAdapter(ArrayAdapter(context, layoutId, documents.data!!.values.toList()))
            } else {
                d("message2", "No such document")
            }
        }
}