package com.packager.packager.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.packager.packager.R

fun ImageView.setImageWithGlide(url: String) {

    Glide.with(this).load(url).placeholder(R.drawable.ic_bnav_home).into(this)

}