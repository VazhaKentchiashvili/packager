package com.packager.packager.extensions

import android.content.Context
import android.view.View
import android.widget.Toast

fun View.invisible() {
    visibility = View.GONE
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.showIf(state: Boolean) {
    if (state)
        visible()
    else
        invisible()
}

fun Context.showToast(message: String) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
