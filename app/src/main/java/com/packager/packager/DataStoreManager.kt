package com.packager.packager

import android.content.Context
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.first
import javax.inject.Inject


private val Context.dataStore by preferencesDataStore("auth_datastore")

class DataStoreManager @Inject constructor(@ApplicationContext appContext: Context) {

    companion object {
        const val SESSION = "EASY_AUTH"
        const val PERSON_PHONE_NUMBER = "PERSON_PHONE_NUMBER"
    }

    private val authDataStore = appContext.dataStore

    suspend fun saveSession(session: Boolean) {
        val easyAuthKey = booleanPreferencesKey(SESSION)
        authDataStore.edit {datastore ->
            datastore[easyAuthKey] = session
        }
    }

    suspend fun readSession(): Boolean {
        val sessionKey = booleanPreferencesKey(SESSION)
        val preferences = authDataStore.data.first()
        return preferences[sessionKey] ?: false
    }

    suspend fun savePersonPhoneNumber(phoneNumber: String) {
        val numberKey = stringPreferencesKey(PERSON_PHONE_NUMBER)
        authDataStore.edit { datastore ->
            datastore[numberKey] = phoneNumber
        }
    }

    suspend fun readPersonPhoneNumber(): String {
        val numberKey = stringPreferencesKey(PERSON_PHONE_NUMBER)
        val preferences = authDataStore.data.first()

        return preferences[numberKey] ?: " "
    }

    suspend fun clearSavedFavoritesPhone() {
        authDataStore.edit {
            val preferenceKey = stringPreferencesKey(PERSON_PHONE_NUMBER)
            it.remove(preferenceKey)
        }
    }

    suspend fun clearDataStore() {
        authDataStore.edit {
            it.clear()
        }
    }
}