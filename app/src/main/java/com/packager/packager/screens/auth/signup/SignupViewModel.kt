package com.packager.packager.screens.auth.signup

import android.util.Log.d
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.AuthResult
import com.google.firebase.firestore.DocumentReference
import com.packager.packager.models.UserModel
import com.packager.packager.repositoy.signUpRepository.SignUpRepoImplement
import com.packager.packager.retrofit.Handler
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SignupViewModel @Inject constructor(
    private val signUpRepoImplement: SignUpRepoImplement
) : ViewModel() {

    private val _registerResponse: MutableLiveData<Handler<AuthResult>> = MutableLiveData()
    val registerResponse: LiveData<Handler<AuthResult>>
        get() = _registerResponse

    fun register(
        email: String,
        password: String,
        status: String
    ) = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            val response = signUpRepoImplement.register(email, password, status)
            _registerResponse.postValue(response)
        }
    }

    private val _uploadDataResponse: MutableLiveData<Handler<DocumentReference>> = MutableLiveData()
    val uploadDataResponse: LiveData<Handler<DocumentReference>>
        get() = _uploadDataResponse

    fun uploadData(
        user: UserModel
    ) = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            val response = signUpRepoImplement.uploadData(user)
            d("myLogs", user.toString())
            _uploadDataResponse.postValue(response)
        }
    }
}