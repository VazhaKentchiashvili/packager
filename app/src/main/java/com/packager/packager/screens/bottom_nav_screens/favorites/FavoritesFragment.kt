package com.packager.packager.screens.bottom_nav_screens.favorites

import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.packager.packager.DataStoreManager
import com.packager.packager.R
import com.packager.packager.adapter.FavoritesAdapter
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.FavoritesFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class FavoritesFragment : BaseFragment<FavoritesFragmentBinding>(FavoritesFragmentBinding::inflate) {

    private val favoritesAdapter = FavoritesAdapter()
    private val viewModel: FavoritesViewModel by viewModels()

    @Inject
    lateinit var firebaseFirestore: FirebaseFirestore

    @Inject
    lateinit var firebaseAuth: FirebaseAuth

    @Inject
    lateinit var dataStoreManager: DataStoreManager

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {
        viewModel.getFavorites()
        getSentAndReceivedParcels()
        setupRecycler()
        sentParcel()
    }

    private fun getSentAndReceivedParcels() {
        observes()
    }



    private fun setupRecycler() {
        binding.recyclerFavorites.apply {
            adapter = favoritesAdapter
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }
    }

    private fun sentParcel() {
        favoritesAdapter.itemClick = { _, phoneNumber ->

            lifecycleScope.launch {
                dataStoreManager.savePersonPhoneNumber(phoneNumber.mobileNumber)

                findNavController().navigate(R.id.action_favoritesFragment_to_sendFragment2)
            }

        }
    }



    private fun observes(){
        viewModel.getFavoritesResponse.observe(viewLifecycleOwner, {
            favoritesAdapter.setData(it)
        })
    }
}