package com.packager.packager.screens.bottom_nav_screens.sendParcel

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.SendFragmentBinding
import com.packager.packager.extensions.showToast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SendFragment : BaseFragment<SendFragmentBinding>(SendFragmentBinding::inflate) {

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
    }
}