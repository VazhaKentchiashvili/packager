package com.packager.packager.screens.profile.myParcels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.packager.packager.models.Parcel
import com.packager.packager.repositoy.my_parcels_repository.MyParcelsRepoImplement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MyParcelsViewModel : ViewModel() {

    private val repo: MyParcelsRepoImplement by lazy { MyParcelsRepoImplement() }
    private val _getMyParcelResponse = MutableLiveData<List<Parcel>>()
    val getMyParcelResponse: LiveData<List<Parcel>>
        get() = _getMyParcelResponse

    fun getMyParcels() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val response = repo.getMyParcels()
                _getMyParcelResponse.postValue(response)
                Log.d("viewModelResponse", response.toString())
            }
        }
    }

}