package com.packager.packager.screens.bottom_nav_screens.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.packager.packager.models.Parcel
import com.packager.packager.repositoy.home_parcels_repository.HomeParcelsRepoImplement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeViewModel : ViewModel() {
    private val repo: HomeParcelsRepoImplement by lazy { HomeParcelsRepoImplement() }
    private val _getHomeParcelResponse = MutableLiveData<List<Parcel>>()
    val getHomeParcelResponse: LiveData<List<Parcel>>
        get() = _getHomeParcelResponse

    fun getHomeParcels() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val response = repo.getHomeParcels()
                _getHomeParcelResponse.postValue(response)

            }
        }
    }
}