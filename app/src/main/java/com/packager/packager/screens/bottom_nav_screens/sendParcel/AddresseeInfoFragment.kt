package com.packager.packager.screens.bottom_nav_screens.sendParcel

import android.text.Editable
import android.text.TextWatcher
import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.packager.packager.R
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.FragmentAddresseeInfoBinding
import com.packager.packager.extensions.isEmail
import com.packager.packager.models.ParcelSendingInfo
import com.packager.packager.screens.AddresseeViewModel
import com.packager.packager.screens.bottom_nav_screens.favorites.FavoritesViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class AddresseeInfoFragment :
    BaseFragment<FragmentAddresseeInfoBinding>(FragmentAddresseeInfoBinding::inflate) {

    @Inject
    lateinit var checkInfoUseCase: CheckInfoUseCase

    private val favoritesViewModel: AddresseeViewModel by viewModels()

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        checkInfoUseCase()
        init()
    }

    private fun init() {
        setData()
        favoritesViewModel.setFavoritesInfo()

        binding.btnNext.root.text = "შემდეგი"
        binding.btnNext.root.textSize = 14f
        binding.btnNext.root.setOnClickListener {
            if (getEnteredInfo().addresseeEmail?.isEmail() == true) {
                saveAddresseeInfo()
            } else {
                showErrorDialog("შეცდომა!", "ელ.ფოსტის ფორმატი არასწორია, სცადეთ ხელახლა!")
            }
        }
        val textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                checkInfoUseCase()
            }

            override fun afterTextChanged(s: Editable?) {

            }
        }
        binding.etNameOfAddressee.addTextChangedListener(textWatcher)
        binding.etLastnameOfAddressee.addTextChangedListener(textWatcher)
        binding.etPersonalNumberOfAddressee.addTextChangedListener(textWatcher)
        binding.etPhoneNumberOfAddressee.addTextChangedListener(textWatcher)
        binding.etEmailOfAddressee.addTextChangedListener(textWatcher)
    }

    private fun saveAddresseeInfo() {

        val parcelDirectionInfo = arguments?.getParcelable<ParcelSendingInfo>("SendParcelInfo")
        val bundle = bundleOf(
            "SendParcelInfo" to ParcelSendingInfo(
                parcelDirectionInfo?.sendFrom,
                parcelDirectionInfo?.sendWhere,
                parcelDirectionInfo?.sendServiceCenter,
                getEnteredInfo().addresseeFirstName,
                getEnteredInfo().addresseeLastName,
                getEnteredInfo().addresseePersonalNumber,
                getEnteredInfo().addresseePhoneNumber,
                getEnteredInfo().addresseeEmail
            )
        )

        findNavController().navigate(
            R.id.action_addresseeInfoFragment_to_parcelInfoFragment,
            bundle
        )
    }

    private fun getEnteredInfo(): ParcelSendingInfo {
        val addresseeFirstName = binding.etNameOfAddressee.text.toString()
        val addresseeLastName = binding.etLastnameOfAddressee.text.toString()
        val addresseePersonalNumber = binding.etPersonalNumberOfAddressee.text.toString()
        val addresseePhoneNumber = binding.etPhoneNumberOfAddressee.text.toString()
        val addresseeEmail = binding.etEmailOfAddressee.text.toString()

        return ParcelSendingInfo(
            addresseeFirstName = addresseeFirstName,
            addresseeLastName = addresseeLastName,
            addresseePersonalNumber = addresseePersonalNumber,
            addresseePhoneNumber = addresseePhoneNumber,
            addresseeEmail = addresseeEmail
        )
    }

    private fun setData() {

        favoritesViewModel.favoritesData.observe(viewLifecycleOwner, {

            if (it.isNotEmpty()) {
                binding.etNameOfAddressee.setText(it[0].name)
                binding.etLastnameOfAddressee.setText(it[0].lastName)
                binding.etEmailOfAddressee.setText(it[0].email)
                binding.etPersonalNumberOfAddressee.setText(it[0].privateNumber)
                binding.etPhoneNumberOfAddressee.setText(it[0].mobileNumber)
            }
        })
    }

    private fun checkInfoUseCase() {
        checkInfoUseCase.isAddresseeInfoEmpty(
            getEnteredInfo(),
            binding.btnNext.root
        )
    }
}