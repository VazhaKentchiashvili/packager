package com.packager.packager.screens.bottom_nav_screens.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.packager.packager.adapter.SentParcelsAdapter
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.HomeFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : BaseFragment<HomeFragmentBinding>(HomeFragmentBinding::inflate) {

    private val sentParcelsAdapter = SentParcelsAdapter()
    private val viewModel: HomeViewModel by viewModels()

    @Inject
    lateinit var firebaseFirestore: FirebaseFirestore

    @Inject
    lateinit var firebaseAuth: FirebaseAuth

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {
        viewModel.getHomeParcels()
        getSentAndReceivedParcels()
        setupRecycler()
    }

    private fun getSentAndReceivedParcels() {
        observes()
    }

    private fun setupRecycler() {
        binding.rvSentParcels.apply {
            adapter = sentParcelsAdapter
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }
    }

    private fun observes(){
        viewModel.getHomeParcelResponse.observe(viewLifecycleOwner, {
            sentParcelsAdapter.setData(it)
        })
    }
}