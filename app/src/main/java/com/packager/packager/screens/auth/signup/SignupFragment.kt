package com.packager.packager.screens.auth.signup


import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.packager.packager.R
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.SignupFragmentBinding
import com.packager.packager.models.UserModel
import com.packager.packager.retrofit.Handler
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignupFragment : BaseFragment<SignupFragmentBinding>(
    SignupFragmentBinding::inflate
) {

    private val viewModel: SignupViewModel by viewModels()

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

        init()
    }

    private fun init() {
        binding.btnRegister.setOnClickListener {
            saveAccount()
        }
        binding.tvAlreadyHaveAnAccount.setOnClickListener {
            findNavController().navigate(R.id.action_signupFragment_to_loginFragment)
        }
        observe()
    }

    private fun saveAccount() {
        val name = binding.etRegisterName.text.toString()
        val lastName = binding.etRegisterLastname.text.toString()
        val email = binding.etRegisterEmail.text.toString()
        val privateNumber = binding.etRegisterPersonalNumber.text.toString()
        val mobileNumber = binding.etRegisterPhoneNumber.text.toString()
        val password = binding.etRegisterPassword.text.toString()
        val repeatPassword = binding.etRegisterRepeatPassword.text.toString()
        val status = "NONE"

        when {
            name.isBlank() -> showErrorDialog("შეცდომა!", "გთხოვთ შეავსოთ სახელის ველი")
            lastName.isBlank() -> showErrorDialog("შეცდომა!", "გთხოვთ შეავსოთ გვარის ველი")
            email.isBlank() -> showErrorDialog("შეცდომა!", "გთხოვთ შეავსოთ იმეილის ველი")

            privateNumber.isBlank() || privateNumber.length < 11 -> showErrorDialog(
                "შეცდომა!",
                "გთხოვთ შეავსოთ პირადი ნომრის ველი"
            )
            mobileNumber.isBlank() -> showErrorDialog(
                "შეცდომა!",
                "გთხოვთ შეავსოთ მობილური ნომრის ველი"
            )
            password.isBlank() || password.length < 6 -> showErrorDialog(
                "შეცდომა!",
                "გთხოვთ შეავსოთ პაროლის ველი(მინ. 6 სიმბოლო)"
            )
            repeatPassword.isBlank() -> showErrorDialog(
                "შეცდომა!",
                "გთხოვთ შეავსოთ განმეორებითი პაროლის ველი"
            )
            password != repeatPassword -> showErrorDialog(
                "შეცდომა!",
                "შეყვანილი პაროლები არ ემთხვევა"
            )
            else -> {
                viewModel.register(email, password, status)
                val user = UserModel(name, lastName, email, privateNumber, mobileNumber)
                viewModel.uploadData(user)
            }
        }
    }


    private fun observe() {

        viewModel.registerResponse.observe(viewLifecycleOwner, {
            when (it) {
                is Handler.Success -> {

                    Toast.makeText(requireContext(), "რეგისტრაცია წარმატებით დასრულდა!", Toast.LENGTH_SHORT).show()
                    findNavController().navigate(R.id.action_signupFragment_to_loginFragment)
                }
                is Handler.Error -> {
                    showErrorDialog("შეცდომა!", "მეილი რეგისტრირებულია ან არ არსებობს!")
                }
            }
        })


        viewModel.uploadDataResponse.observe(viewLifecycleOwner, {
            when (it) {
                is Handler.Success -> {
                    Log.d("RESPONSE3", "${it.data}")
                }
                is Handler.Error -> {
                    Log.d("RESPONSE4", "${it.errorMessage}")
                }
            }
        })
    }
}