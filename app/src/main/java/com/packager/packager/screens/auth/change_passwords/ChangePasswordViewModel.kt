package com.packager.packager.screens.auth.change_passwords

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.Task
import com.packager.packager.repositoy.manage_password_repository.ManagePasswordRepoImplement
import com.packager.packager.retrofit.Handler
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ChangePasswordViewModel@Inject constructor(
    private val managePasswordRepoImplement: ManagePasswordRepoImplement
)  : ViewModel() {

    private val _changePassResponse: MutableLiveData<Handler<Task<Void>>> = MutableLiveData()
    val changePassResponse: LiveData<Handler<Task<Void>>>
        get() = _changePassResponse

    fun changePassword(password:String) = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            val response = managePasswordRepoImplement.changePassword(password)
            _changePassResponse.postValue(response)
        }
    }
}