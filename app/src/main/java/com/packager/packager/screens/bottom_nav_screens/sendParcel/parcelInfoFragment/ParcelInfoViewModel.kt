package com.packager.packager.screens.bottom_nav_screens.sendParcel.parcelInfoFragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.DocumentReference
import com.packager.packager.models.FavoritesModel
import com.packager.packager.models.Parcel
import com.packager.packager.repositoy.favorites_repository.FavoritesRepoImplement
import com.packager.packager.repositoy.uploadParcelRepository.UploadParcelRepoImplement
import com.packager.packager.retrofit.Handler
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ParcelInfoViewModel @Inject constructor(
    private val favoritesRepoImplement: FavoritesRepoImplement,
    private val uploadParcelRepoImplement: UploadParcelRepoImplement,
) : ViewModel() {

    private val _uploadParcelResponse: MutableLiveData<Handler<DocumentReference>> = MutableLiveData()
    val uploadParcelResponse: LiveData<Handler<DocumentReference>>
        get() = _uploadParcelResponse

    fun uploadData(
        parcel: Parcel
    ) = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            val response = uploadParcelRepoImplement.uploadParcel(parcel)
            _uploadParcelResponse.postValue(response)
        }
    }


    private val _uploadFavoritesResponse: MutableLiveData<Handler<DocumentReference>> = MutableLiveData()
    val uploadFavoritesResponse: LiveData<Handler<DocumentReference>>
    get() = _uploadFavoritesResponse

    fun uploadFavorites(
        favorites: FavoritesModel
    ) = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            val response = favoritesRepoImplement.uploadFavorites(favorites)
            _uploadFavoritesResponse.postValue(response)
        }
    }
}