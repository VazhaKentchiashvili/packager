package com.packager.packager.screens.bottom_nav_screens.sendParcel.parcel_direction

import android.util.Log.d
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.packager.packager.models.Geocoding
import com.packager.packager.repositoy.parcel_location_repository.ParcelLocationRepoImpl
import com.packager.packager.utils.Constants.MAP_API_KEY
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class ParcelDirectionViewModel @Inject constructor(
    private val parcelLocationRepoImpl: ParcelLocationRepoImpl
) : ViewModel() {
    private val mainDispatcher: CoroutineDispatcher = Dispatchers.Main

    private val _parcelLocationLiveData = MutableLiveData<List<Geocoding.Result>>()
    val parcelLocationLiveData: LiveData<List<Geocoding.Result>>
        get() = _parcelLocationLiveData

    private val _serviceCenterLocation = MutableLiveData<List<Geocoding.Result>>()
    val serviceCenterLocation: LiveData<List<Geocoding.Result>>
        get() = _serviceCenterLocation

    fun getParcelLocationWithLatLng(latLang: String) {

        viewModelScope.launch(mainDispatcher) {
            withContext(mainDispatcher) {
                val response = parcelLocationRepoImpl.getParcelLocation(
                    latLang,
                    MAP_API_KEY
                )
                _parcelLocationLiveData.postValue(response.data?.results!!)
            }
        }
    }

    fun getServiceCenterLocation(address: String) {
        viewModelScope.launch(mainDispatcher) {
            withContext(mainDispatcher) {
                val response = parcelLocationRepoImpl.getServiceCenterLocation(
                    address,
                    MAP_API_KEY
                )
                _serviceCenterLocation.postValue(response.data?.results!!)
            }
        }
    }
}
