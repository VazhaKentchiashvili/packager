package com.packager.packager.screens.bottom_nav_screens.sendParcel.parcel_direction

import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.FirebaseFirestore
import com.packager.packager.R
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.FragmentParcelDirectionBinding
import com.packager.packager.extensions.getDropdownItems
import com.packager.packager.extensions.invisible
import com.packager.packager.extensions.showToast
import com.packager.packager.models.ParcelSendingInfo
import com.packager.packager.screens.bottom_nav_screens.sendParcel.CheckInfoUseCase
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ParcelDirectionFragment :
    BaseFragment<FragmentParcelDirectionBinding>(FragmentParcelDirectionBinding::inflate),
    OnMapReadyCallback {

    private lateinit var map: GoogleMap

    @Inject
    lateinit var firebaseFirestore: FirebaseFirestore

    private val allServiceCentersViewModel: ParcelDirectionViewModel by viewModels()

    @Inject
    lateinit var checkInfoUseCase: CheckInfoUseCase

    private var parcelSendFrom: String = " "

    private lateinit var fusedLocationProvider: FusedLocationProviderClient
    private lateinit var lastLocation: Location

    private val permissionRequest =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
            if (it[Manifest.permission.ACCESS_FINE_LOCATION] == true && it[Manifest.permission.ACCESS_COARSE_LOCATION] == true
            ) {
                getCurrentLocation()
            }
        }

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {
        fusedLocationProvider = LocationServices.getFusedLocationProviderClient(requireContext())
        firebaseFirestore = FirebaseFirestore.getInstance()
        permissionsRequest(permissionRequest)

        checkEnteredInfo()

        observer()

        binding.atServiceCenter.setOnItemClickListener { _, _, _, _ ->
            checkEnteredInfo()
            val serviceCenterAddress: String? = binding.atServiceCenter.text?.toString()

            if (serviceCenterAddress!!.isNotEmpty()) {
                allServiceCentersViewModel.getServiceCenterLocation(serviceCenterAddress)
            }
        }

        binding.btnNext.root.text = "შემდეგი"
        binding.btnNext.root.textSize = 14f
        binding.btnNext.root.setOnClickListener {
            saveParcelDirectionInfo()
            binding.atSendParcelWhere.text.clear()
            binding.atServiceCenter.text.clear()
        }

        parcelDirections()

        binding.atSendParcelWhere.setOnItemClickListener { _, _, _, _ ->
            serviceCentersInCountries()
            checkEnteredInfo()

            binding.atServiceCenter.text.clear()

        }

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }

    private fun checkEnteredInfo() {
        val sendParcelFrom = binding.atSendParcelFrom.text.toString()
        val sendParcelWhere = binding.atSendParcelWhere.text.toString()
        val sendParcelServiceCenter = binding.atServiceCenter.text.toString()

        checkInfoUseCase.isParcelDirectionEmpty(
            sendParcelFrom,
            sendParcelWhere,
            sendParcelServiceCenter,
            binding.btnNext.root
        )
    }

    private fun parcelDirections() {

        // Send parcel where. (countries)
        firebaseFirestore.getDropdownItems(
            binding.atSendParcelWhere,
            requireContext(),
            R.layout.dropdown_item_view,
            collectionPath = "sendFragment",
            documentPath = "send_parcel_where",
        )
    }

    // This method put all service centers in the dropdown menu for each chosen countries.
    private fun serviceCentersInCountries() {
        val chosenCountry = binding.atSendParcelWhere.text.toString()

        firebaseFirestore.getDropdownItems(
            binding.atServiceCenter,
            requireContext(),
            R.layout.dropdown_item_view,
            collectionPath = "sendFragment",
            documentPath = chosenCountry,
        )
    }


    private fun saveParcelDirectionInfo() {
        parcelSendFrom
        val sendParcelWhere = binding.atSendParcelWhere.text.toString()
        val sendParcelServiceCenter = binding.atServiceCenter.text.toString()

        val bundle = bundleOf(
            "SendParcelInfo" to ParcelSendingInfo(
                parcelSendFrom,
                sendParcelWhere,
                sendParcelServiceCenter
            )
        )
        findNavController().navigate(
            R.id.action_parcelDirectionFragment_to_addresseeInfoFragment,
            bundle
        )
    }


    @SuppressLint("MissingPermission")
    private fun getCurrentLocation() {
        fusedLocationProvider.lastLocation.addOnSuccessListener {
            if (it != null) {

                map.isMyLocationEnabled = true
                lastLocation = it
                val currLatLng = LatLng(lastLocation.latitude, lastLocation.longitude)

                map.moveCamera(CameraUpdateFactory.newLatLngZoom(currLatLng, 17f))

            }
        }

    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        map.setOnCameraIdleListener {
            binding.imLocationPointer.animate().translationY(0f)
            if (binding.atSendParcelWhere.text.isEmpty()) {
                allServiceCentersViewModel
                    .getParcelLocationWithLatLng(map.cameraPosition.target.latitude.toString() + "," + map.cameraPosition.target.longitude.toString())

            } else {
                binding.imLocationPointer.invisible()
            }
        }

        googleMap.setOnCameraMoveStartedListener {
            binding.imLocationPointer.animate().translationY(-50f)
        }
    }

    private fun observer() {
        allServiceCentersViewModel.parcelLocationLiveData.observe(viewLifecycleOwner, {

            if (it.isNotEmpty()) {
                Toast.makeText(requireContext(), it[0].formatted_address, Toast.LENGTH_SHORT).show()
                binding.atSendParcelFrom.setText(it[0].formatted_address)

                parcelSendFrom = it[0].address_components?.get(4)?.short_name!!
            }
        })

        allServiceCentersViewModel.serviceCenterLocation.observe(viewLifecycleOwner, {

            if (it.isNotEmpty()) {
                val lat = it[0].geometry!!.location!!.lat
                val lng = it[0].geometry!!.location!!.lng

                val latLng = LatLng(lat!!, lng!!)
                map.moveCamera(CameraUpdateFactory.newLatLng(latLng))
            }

        })
    }
}