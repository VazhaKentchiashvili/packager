package com.packager.packager.screens.bottom_nav_screens.sendParcel.rules_dialog

import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.packager.packager.adapter.ConditionsAdapter
import com.packager.packager.databinding.FragmentDialogConditionsBinding
import com.packager.packager.retrofit.Handler
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ConditionsDialogFragment : BottomSheetDialogFragment() {

    private val conditionsViewModel: ConditionsViewModel by viewModels()

    private val conditionsAdapter = ConditionsAdapter()

    private var _binding: FragmentDialogConditionsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentDialogConditionsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        init()
    }

    private fun init() {
        setupRecycler()
        conditionsViewModel.getConditions()
        observer()
    }

    private fun observer() {
        conditionsViewModel.conditionsLiveData.observe(viewLifecycleOwner, {
            when(it) {
                is Handler.Success -> conditionsAdapter.setData(it.data!!.conditions.toMutableList())

                is Handler.Error -> d("Error", "Failed")
            }
        })
    }

    private fun setupRecycler() {
        binding.rvConditions.apply {
            adapter = conditionsAdapter
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}