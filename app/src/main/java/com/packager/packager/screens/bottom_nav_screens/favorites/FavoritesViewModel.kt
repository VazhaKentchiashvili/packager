package com.packager.packager.screens.bottom_nav_screens.favorites

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.packager.packager.models.FavoritesModel
import com.packager.packager.repositoy.favorites_repository.FavoritesRepoImplement
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class FavoritesViewModel @Inject constructor(
    private val favoritesRepoImplement: FavoritesRepoImplement
) : ViewModel() {

    private val _getFavoritesResponse = MutableLiveData<List<FavoritesModel>>()
    val getFavoritesResponse: LiveData<List<FavoritesModel>>
        get() = _getFavoritesResponse


    fun getFavorites() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val response = favoritesRepoImplement.getFavorites()
                _getFavoritesResponse.postValue(response)
            }
        }
    }


}