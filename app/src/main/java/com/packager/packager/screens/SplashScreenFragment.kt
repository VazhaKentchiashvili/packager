package com.packager.packager.screens

import android.animation.Animator
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.packager.packager.DataStoreManager
import com.packager.packager.R
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.FragmentSplashScreenBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SplashScreenFragment :
    BaseFragment<FragmentSplashScreenBinding>(FragmentSplashScreenBinding::inflate) {

    @Inject
    lateinit var dataStoreManager: DataStoreManager

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

        init()

    }

    private fun init() {

        binding.lottieAnim.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator?) {

            }

            override fun onAnimationEnd(animation: Animator?) {
                openFragment()
            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationRepeat(animation: Animator?) {

            }
        })
    }

    private fun openFragment() {
        lifecycleScope.launch {
            if (dataStoreManager.readSession()) {
                findNavController().navigate(R.id.action_splashScreenFragment_to_navigationFragment2)
            } else {
                findNavController().navigate(R.id.action_splashScreenFragment_to_loginFragment)
            }
        }
    }
}