package com.packager.packager.screens

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.packager.packager.models.FavoritesModel
import com.packager.packager.repositoy.favorites_repository.FavoritesRepoImplement
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class AddresseeViewModel @Inject constructor(
    private val favoritesRepoImplement: FavoritesRepoImplement
) : ViewModel() {

    private val _favoritesData = MutableLiveData<List<FavoritesModel>>()
    val favoritesData: LiveData<List<FavoritesModel>>
        get() = _favoritesData

    fun setFavoritesInfo() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val response = favoritesRepoImplement.setFavoritesData()
                _favoritesData.postValue(response)
            }
        }
    }
}