package com.packager.packager.screens.profile

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.packager.packager.DataStoreManager
import com.packager.packager.R
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.ProfileFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment : BaseFragment<ProfileFragmentBinding>(ProfileFragmentBinding::inflate) {

    private val viewModel: ProfileViewModel by viewModels()

    @Inject
    lateinit var dataStoreManager: DataStoreManager

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {

        binding.btnSignOut.setOnClickListener {
            signOut()
            val navController = requireActivity().supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
            navController.navController.navigate(R.id.action_global_loginFragment)
        }

        binding.tvMyParcels.setOnClickListener {
            val navController = requireActivity().supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
            navController.navController.navigate(R.id.action_global_myParcelsFragment)
        }

        binding.tvChangePass.setOnClickListener{
            val navController = requireActivity().supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
            navController.navController.navigate(R.id.action_global_changePasswordFragment)
        }

    }

    private fun signOut() {
        lifecycleScope.launch {
            dataStoreManager.clearDataStore()
        }
        viewModel.logOut()
    }
}