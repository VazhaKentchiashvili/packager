package com.packager.packager.screens.auth.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.AuthResult
import com.packager.packager.DataStoreManager
import com.packager.packager.repositoy.logInRepository.LogInRepoImplement
import com.packager.packager.retrofit.Handler
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val logInRepoImplement: LogInRepoImplement
) : ViewModel() {


    private val _logInResponse: MutableLiveData<Handler<AuthResult>> = MutableLiveData()
    val logInResponse: LiveData<Handler<AuthResult>>
        get() = _logInResponse

    fun logIn(email: String, password: String, session: Boolean) = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            val response = logInRepoImplement.logIn(email, password, session)
            _logInResponse.postValue(response)
        }
    }

}