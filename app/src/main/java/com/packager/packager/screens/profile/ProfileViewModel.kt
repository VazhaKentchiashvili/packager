package com.packager.packager.screens.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.AuthResult
import com.packager.packager.DataStoreManager
import com.packager.packager.repositoy.logInRepository.LogInRepoImplement
import com.packager.packager.retrofit.Handler
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel@Inject constructor(
    private val logInRepoImplement: LogInRepoImplement
)  : ViewModel() {

    private val logOutResponse: MutableLiveData<Handler<Unit>> = MutableLiveData()

    fun logOut() = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            val response = logInRepoImplement.logOut()
            logOutResponse.postValue(response)
        }
    }
}