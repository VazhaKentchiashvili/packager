package com.packager.packager.screens.bottom_nav_screens.calculator

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.google.firebase.firestore.FirebaseFirestore
import com.packager.packager.R
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.CalculatorFragmentBinding
import com.packager.packager.extensions.getDropdownItems
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CalculatorFragment : BaseFragment<CalculatorFragmentBinding>(CalculatorFragmentBinding::inflate) {

    @Inject
    lateinit var firebaseFirestore: FirebaseFirestore

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {

        binding.btnCalculate.setOnClickListener {
            calculateParcelPrice()
        }

        sendParcelsFrom()
        setDropdownItemsAdapter()
    }

    private fun sendParcelsFrom() {

        firebaseFirestore.getDropdownItems(
            binding.atSendParcelFrom,
            requireContext(),
            R.layout.dropdown_item_view,
            collectionPath = "sendFragment",
            documentPath = "send_parcel_from",
        )

        firebaseFirestore.getDropdownItems(
            binding.atSendParcelWhere,
            requireContext(),
            R.layout.dropdown_item_view,
            collectionPath = "sendFragment",
            documentPath = "send_parcel_where",
        )
    }

    private fun setDropdownItemsAdapter() {
        val parcelWeight = resources.getStringArray(R.array.parcelWeight)
        val parcelVolume = resources.getStringArray(R.array.parcelVolume)
        val parcelTransport = resources.getStringArray(R.array.parcelTransport)

        val parcelWeightAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_item_view, parcelWeight)
        val parcelVolumeAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_item_view, parcelVolume)
        val parcelTransportAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_item_view, parcelTransport)

        binding.atParcelWeight.setAdapter(parcelWeightAdapter)
        binding.atParcelVolume.setAdapter(parcelVolumeAdapter)
        binding.atTransport.setAdapter(parcelTransportAdapter)

    }

    private fun calculateParcelPrice() {
        val parcelWeight = binding.atParcelWeight.text.dropLast(2).toString().toFloat()
        val parcelVolume = binding.atParcelVolume.text.dropLast(2).toString().toInt()
        val parcelTransport = binding.atTransport.text.dropLast(3).toString().toInt()
        val parcelValue = binding.atParcelValue.text.toString().toInt()
        val calculatedPrice = parcelWeight * parcelVolume + parcelTransport + parcelValue / 2

        binding.tvCalcPriceNumbers.text = calculatedPrice.toString()

    }
}