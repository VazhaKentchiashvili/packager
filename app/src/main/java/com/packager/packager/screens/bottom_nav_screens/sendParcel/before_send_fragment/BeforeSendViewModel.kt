package com.packager.packager.screens.bottom_nav_screens.sendParcel.before_send_fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.packager.packager.models.News
import com.packager.packager.repositoy.news_repository.NewsRepoImpl
import com.packager.packager.retrofit.Handler
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class BeforeSendViewModel @Inject constructor(
    private val newsRepo: NewsRepoImpl
): ViewModel() {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    private val _newsMutableLiveData = MutableLiveData<Handler<News>>()
    val news: LiveData<Handler<News>> get() = _newsMutableLiveData

    fun initViewModel() {

        getNews()
    }

    private fun getNews() {
        viewModelScope.launch {
            withContext(ioDispatcher) {
                val response = newsRepo.getNews()
                _newsMutableLiveData.postValue(response)
            }
        }
    }
}