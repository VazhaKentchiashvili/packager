package com.packager.packager.screens.bottom_nav_screens.home

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.packager.packager.models.Parcel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class ParcelsUseCase @Inject constructor(
    private val firebaseFirestore: FirebaseFirestore,
    private val firebaseAuth: FirebaseAuth
) {
    private val sentParcels = mutableListOf<Parcel>()

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    fun getSentAndReceivedParcels(): List<Parcel> {
        CoroutineScope(ioDispatcher).launch {
            firebaseFirestore.collection("sendParcels")
                .get()
                .addOnCompleteListener {

                    if (it.isSuccessful) {
                        for (document in it.result!!) {
                            Log.d("TAG2", document.data["parcelReceiver"].toString())

                            if (document.data["parcelSender"] == firebaseAuth.currentUser!!.uid) {
                                sentParcels.add(
                                    Parcel(
                                        document.data["parcelSender"].toString(),
                                        document.data["parcelReceiver"].toString(),
                                        document.data["sendFrom"].toString(),
                                        document.data["sendWhere"].toString(),
                                        document.data["email"].toString()
                                    )
                                )
                            }
                        }
                    }
                }
        }
        return sentParcels
    }
}

