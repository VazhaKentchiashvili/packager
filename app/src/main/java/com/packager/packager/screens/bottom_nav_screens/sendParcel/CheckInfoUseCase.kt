package com.packager.packager.screens.bottom_nav_screens.sendParcel

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.packager.packager.models.ParcelSendingInfo

class CheckInfoUseCase {

    fun isParcelDirectionEmpty(
        sendParcelFrom: String,
        sendParcelWhere: String,
        serviceCenter: String,
        view: View,
    ) {
        if (sendParcelFrom.isBlank() ||
            sendParcelWhere.isBlank() ||
            serviceCenter.isBlank()
        ) {
            view.alpha = 0.5f
            view.isEnabled = false
        } else {
            view.alpha = 1f
            view.isEnabled = true
        }
    }


    fun isAddresseeInfoEmpty(
        enteredAddresseeInfo: ParcelSendingInfo,
        view: View
    ) {
        if (enteredAddresseeInfo.addresseeFirstName!!.isBlank() ||
            enteredAddresseeInfo.addresseeLastName!!.isBlank() ||
            enteredAddresseeInfo.addresseePersonalNumber!!.isBlank() ||
            enteredAddresseeInfo.addresseePhoneNumber!!.isBlank()
        ) {
            view.alpha = 0.5f
            view.isEnabled = false
        } else {
            view.alpha = 1f
            view.isEnabled = true
        }
    }

    fun isParcelInfoEmpty(
        enteredParcelInfo: ParcelSendingInfo,
        view: View
    ) {
        if (enteredParcelInfo.parcelWeight!!.isBlank() ||
            enteredParcelInfo.parcelVolume!!.isBlank() ||
            enteredParcelInfo.parcelValue!!.isBlank() ||
            enteredParcelInfo.parcelTransportTime!!.isBlank() ||
            enteredParcelInfo.isConditionsAgreed == false
        ) {
            view.isEnabled = false
            view.alpha = 0.5f
        } else {
            view.isEnabled = true
            view.alpha = 1f
        }
    }
}