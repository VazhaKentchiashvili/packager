package com.packager.packager.screens.auth.change_passwords

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.packager.packager.R
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.ChangePasswordFragmentBinding
import com.packager.packager.retrofit.Handler
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChangePasswordFragment :
    BaseFragment<ChangePasswordFragmentBinding>(ChangePasswordFragmentBinding::inflate) {

    private val viewModel: ChangePasswordViewModel by viewModels()

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }


    private fun init() {
        binding.btnChangePass.setOnClickListener {
            changePassword()
            observe()
        }

    }


    private fun changePassword() {
        val password = binding.etRegisterPassword.text.toString()
        val repeatPassword = binding.etChangeRepeatPassword.text.toString()

        if (password.isEmpty() || repeatPassword.isEmpty()) {
            showErrorDialog("შეცდომა!", "გთხოვთ შეავსეთ ყველა ველი")
        } else if (password != repeatPassword) {
            showErrorDialog("შეცდომა!", "შეყვანილი პარლები არ ემთხვევა ერთმანეთს")

        }else{
            viewModel.changePassword(password)
        }


    }

    private fun observe() {
        viewModel.changePassResponse.observe(viewLifecycleOwner, {
            when (it) {
                is Handler.Success -> {
                    Toast.makeText(
                        requireContext(),
                        "პაროლი წარმატებით შეიცვალა!",
                        Toast.LENGTH_SHORT
                    ).show()
                    val navController =
                        requireActivity().supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
                    navController.navController.navigate(R.id.action_global_navigationFragment2)
                }
                is Handler.Error -> {
                    showErrorDialog(
                        "შეცდომა!",
                        "პაროლი ვერ შეიცვალა"
                    )
                    Log.d("RESPONSE ", "${it.errorMessage}")
                }

            }
        })
    }


}