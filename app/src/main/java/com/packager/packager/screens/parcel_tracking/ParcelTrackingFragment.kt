package com.packager.packager.screens.parcel_tracking

import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.ParcelTrackingFragmentBinding

class ParcelTrackingFragment : BaseFragment<ParcelTrackingFragmentBinding>(ParcelTrackingFragmentBinding::inflate) {

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

    }
}