package com.packager.packager.screens.bottom_nav_screens.sendParcel.before_send_fragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import com.packager.packager.R
import com.packager.packager.adapter.NewsSliderAdapter
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.FragmentBeforeSendParcelBinding
import com.packager.packager.extensions.showToast
import com.packager.packager.retrofit.Handler
import dagger.hilt.android.AndroidEntryPoint
import kotlin.math.abs

@AndroidEntryPoint
class BeforeSendParcelFragment :
    BaseFragment<FragmentBeforeSendParcelBinding>(FragmentBeforeSendParcelBinding::inflate) {

    private val pagerAdapter = NewsSliderAdapter()
    private val newsViewModel: BeforeSendViewModel by viewModels()
    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {
        newsViewModel.initViewModel()
        initSliderAdapter()
        observer()

        binding.btnStartSending.root.text = "გაგზავნე ამანათი"

        binding.btnStartSending.root.setOnClickListener {
            findNavController().navigate(R.id.action_beforeSendParcelFragment_to_parcelDirectionFragment)
        }

    }

    private fun observer() {
        newsViewModel.news.observe(viewLifecycleOwner, {
            when(it) {
                is Handler.Success -> {
                    pagerAdapter.setData(it.data?.news!!.toMutableList())
                }
                is Handler.Error -> {
                    requireContext().showToast("Request failed")
                }
            }
        })
    }

    private fun initSliderAdapter() {

        binding.vpSlider.clipToPadding = false
        binding.vpSlider.clipChildren = false
        binding.vpSlider.offscreenPageLimit = 3

        val composePageTrans = CompositePageTransformer()
        composePageTrans.addTransformer(MarginPageTransformer(40))
        composePageTrans.addTransformer { page, position ->
            val r: Float = 1 - abs(position)
            page.scaleY = 0.55f + r * 0.55f
        }

        binding.vpSlider.setPageTransformer(composePageTrans)
        binding.vpSlider.adapter = pagerAdapter

    }
}