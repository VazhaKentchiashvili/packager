package com.packager.packager.screens.auth.login

import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.packager.packager.R
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.LoginFragmentBinding
import com.packager.packager.retrofit.Handler
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : BaseFragment<LoginFragmentBinding>(
    LoginFragmentBinding::inflate
) {

    private val viewModel: LoginViewModel by viewModels()

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

        init()

    }

    private fun init() {
        binding.btnLogin.setOnClickListener {
            logIn()
        }

        binding.tvDontHaveAnAccount.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_signupFragment)
        }

        observe()
    }


    private fun logIn() {
        val email = binding.etAuthEmail.text.toString()
        val pass = binding.etAuthPassword.text.toString()
        val session = binding.checkBox.isChecked

        if (email.isEmpty() || pass.isEmpty()) {
            showErrorDialog("შეცდომა!", "გთხოვთ შეავსეთ ყველა ველი")
        } else {
            if (pass.isNotBlank()) {
                viewModel.logIn(
                    email,
                    pass,
                    session
                )
            }
        }
    }

    private fun observe() {
        viewModel.logInResponse.observe(viewLifecycleOwner, {
            when (it) {
                is Handler.Success -> {
                    findNavController().navigate(R.id.action_loginFragment_to_navigationFragment2)
                    d("RESPONSE", "${it.data}")
                }
                is Handler.Error -> {
                    showErrorDialog(
                        "მომხმარებლის ანგარიში ვერ მოიძებნა",
                        "გთხოვთ გადაამოწმოთ შეყვანილი ველები"
                    )
                    d("RESPONSE ", "${it.errorMessage}")
                }

            }
        })
    }
}