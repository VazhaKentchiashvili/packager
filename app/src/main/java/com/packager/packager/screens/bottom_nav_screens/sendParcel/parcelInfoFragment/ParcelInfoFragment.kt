package com.packager.packager.screens.bottom_nav_screens.sendParcel.parcelInfoFragment

import android.app.Dialog
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListAdapter
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.packager.packager.DataStoreManager
import com.packager.packager.R
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.FragmentParcelInfoBinding
import com.packager.packager.extensions.getDropdownItems
import com.packager.packager.extensions.startUp
import com.packager.packager.models.FavoritesModel
import com.packager.packager.models.Parcel
import com.packager.packager.models.ParcelSendingInfo
import com.packager.packager.screens.bottom_nav_screens.sendParcel.CheckInfoUseCase
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ParcelInfoFragment :
    BaseFragment<FragmentParcelInfoBinding>(FragmentParcelInfoBinding::inflate) {

    private val viewModel: ParcelInfoViewModel by viewModels()
    private lateinit var parcelDirectionInfo: ParcelSendingInfo

    @Inject
    lateinit var firebaseFirestore: FirebaseFirestore

    @Inject
    lateinit var checkInfoUseCase: CheckInfoUseCase

    @Inject
    lateinit var dataStoreManager: DataStoreManager

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {
        parcelDirectionInfo = arguments?.getParcelable("SendParcelInfo")!!

        checkInfoUseCase()
        initTextWatcher()
        initListeners()
        setDropdownItemsAdapter()

    }

    private fun initListeners() {
        binding.btnSendParcel.root.text = "გაგზავნა"
        binding.btnSendParcel.root.textSize = 14f
        binding.btnSendParcel.root.setOnClickListener {

            showQuestionDialog("გაგზავნა", "გსურთ მომხმარებლის ფავორიტებში დამატება?")
            lifecycleScope.launch {
                dataStoreManager.clearSavedFavoritesPhone()
            }
        }

        binding.chbAgreement.setOnClickListener {
            checkInfoUseCase()
            calculateParcelPrice()
        }

        binding.tvAgreement.setOnClickListener {
            if (findNavController().currentDestination?.id == R.id.parcelInfoFragment) {
                findNavController().navigate(R.id.action_parcelInfoFragment_to_itemListDialogFragment)
            }
        }
    }

    private fun initTextWatcher() {
        val textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                checkInfoUseCase()
            }

            override fun afterTextChanged(s: Editable?) {

            }
        }
        with(binding) {
            atParcelWeight.addTextChangedListener(textWatcher)
            atParcelValue.addTextChangedListener(textWatcher)
            atParcelVolume.addTextChangedListener(textWatcher)
            atTransport.addTextChangedListener(textWatcher)
        }
    }

    private fun setDropdownItemsAdapter() {
        val parcelWeight = resources.getStringArray(R.array.parcelWeight)
        val parcelVolume = resources.getStringArray(R.array.parcelVolume)
        val parcelTransport = resources.getStringArray(R.array.parcelTransport)

        val parcelWeightAdapter =
            ArrayAdapter(requireContext(), R.layout.dropdown_item_view, parcelWeight)
        val parcelVolumeAdapter =
            ArrayAdapter(requireContext(), R.layout.dropdown_item_view, parcelVolume)
        val parcelTransportAdapter =
            ArrayAdapter(requireContext(), R.layout.dropdown_item_view, parcelTransport)

        binding.atParcelWeight.setAdapter(parcelWeightAdapter)
        binding.atParcelVolume.setAdapter(parcelVolumeAdapter)
        binding.atTransport.setAdapter(parcelTransportAdapter)

    }

    private fun getParcelInfo() {
        val bundle = bundleOf(
            "SendParcelInfo" to ParcelSendingInfo(
                parcelDirectionInfo.sendFrom,
                parcelDirectionInfo.sendWhere,
                parcelDirectionInfo.sendServiceCenter,
                parcelDirectionInfo.addresseeFirstName,
                parcelDirectionInfo.addresseeLastName,
                parcelDirectionInfo.addresseePersonalNumber,
                parcelDirectionInfo.addresseePhoneNumber,
                parcelDirectionInfo.addresseeEmail,
                getEnteredParcelInfo().parcelWeight.toString(),
                getEnteredParcelInfo().parcelValue.toString(),
                getEnteredParcelInfo().parcelVolume.toString(),
                getEnteredParcelInfo().parcelTransportTime.toString()
            )
        )
        findNavController().navigate(R.id.action_parcelInfoFragment_to_main_navigation, bundle)
    }

    private fun uploadParcel() {

        val firebase = FirebaseAuth.getInstance()

        val parcel = Parcel(
            firebase.currentUser!!.uid,
            parcelDirectionInfo.addresseePersonalNumber.toString(),
            parcelDirectionInfo.sendFrom.toString(),
            parcelDirectionInfo.sendWhere.toString(),
            firebase.currentUser!!.email.toString()
        )

        viewModel.uploadData(parcel)
    }

    private fun uploadFavorites() {

        val firebase = FirebaseAuth.getInstance()

        val favorites = FavoritesModel(
            parcelDirectionInfo.addresseeFirstName.toString(),
            parcelDirectionInfo.addresseeLastName.toString(),
            parcelDirectionInfo.addresseeEmail.toString(),
            parcelDirectionInfo.addresseePersonalNumber.toString(),
            parcelDirectionInfo.addresseePhoneNumber.toString(),
            firebase.currentUser!!.uid
        )

        viewModel.uploadFavorites(favorites)

    }

    private fun getEnteredParcelInfo(): ParcelSendingInfo {
        val parcelWeight = binding.atParcelWeight.text.toString()
        val parcelValue = binding.atParcelValue.text.toString()
        val parcelVolume = binding.atParcelVolume.text.toString()
        val parcelTransport = binding.atTransport.text.toString()
        val isConditionsAgreed = binding.chbAgreement.isChecked

        return ParcelSendingInfo(
            parcelWeight = parcelWeight,
            parcelValue = parcelValue,
            parcelVolume = parcelVolume,
            parcelTransportTime = parcelTransport,
            isConditionsAgreed = isConditionsAgreed
        )
    }

    private fun checkInfoUseCase() {
        checkInfoUseCase.isParcelInfoEmpty(
            getEnteredParcelInfo(),
            binding.btnSendParcel.root
        )
    }

    private fun calculateParcelPrice() {
        val parcelWeight = binding.atParcelWeight.text.dropLast(2).toString().toFloat()
        val parcelVolume = binding.atParcelVolume.text.dropLast(2).toString().toInt()
        val parcelTransport = binding.atTransport.text.dropLast(3).toString().toInt()
        val parcelValue = binding.atParcelValue.text.toString().toInt()
        val calculatedPrice = parcelWeight * parcelVolume + parcelTransport + parcelValue / 2

        binding.tvParcelTransportPrice.text = calculatedPrice.toString()
    }

    private fun showQuestionDialog(title: String, description: String) {

        val dialog = Dialog(requireContext())
        dialog.startUp(R.layout.dialog_layout_favorites)
        dialog.findViewById<TextView>(R.id.dialogFavoritesTitle).text = title
        dialog.findViewById<TextView>(R.id.dialogFavoritesDescription).text = description

        dialog.findViewById<Button>(R.id.btnFavoritesCloseDialog).text = "არა"
        dialog.findViewById<Button>(R.id.btnFavoritesAcceptDialog).text = "კი"

        dialog.findViewById<Button>(R.id.btnFavoritesCloseDialog).setOnClickListener {
            dialog.cancel()
            getParcelInfo()
            uploadParcel()
        }

        dialog.findViewById<Button>(R.id.btnFavoritesAcceptDialog).setOnClickListener {
            dialog.cancel()
            getParcelInfo()
            uploadParcel()
            uploadFavorites()
            lifecycleScope.launch {
                getEnteredParcelInfo().addresseePhoneNumber?.let { phoneNumber ->
                    dataStoreManager.savePersonPhoneNumber(
                        phoneNumber
                    )
                }
            }
        }

        dialog.show()
    }
}