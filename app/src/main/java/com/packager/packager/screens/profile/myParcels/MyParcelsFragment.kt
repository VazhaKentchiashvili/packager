package com.packager.packager.screens.profile.myParcels


import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.packager.packager.adapter.MyParcelsAdapter
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.MyParcelsFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MyParcelsFragment :
    BaseFragment<MyParcelsFragmentBinding>(MyParcelsFragmentBinding::inflate) {
    private val receiveParcelsAdapter = MyParcelsAdapter()
    private val viewModel: MyParcelsViewModel by viewModels()

    @Inject
    lateinit var firebaseFirestore: FirebaseFirestore

    @Inject
    lateinit var firebaseAuth: FirebaseAuth

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {
        viewModel.getMyParcels()
        setupRecycler()
        getSentAndReceivedParcels()
    }

    private fun getSentAndReceivedParcels() {
        observes()
    }

    private fun setupRecycler() {
        binding.recyclerMyParcels.apply {
            adapter = receiveParcelsAdapter
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }
    }

    private fun observes(){
        viewModel.getMyParcelResponse.observe(viewLifecycleOwner, {
            receiveParcelsAdapter.setData(it)
            Log.d("myAdapter", it.toString())
        })
    }
}

