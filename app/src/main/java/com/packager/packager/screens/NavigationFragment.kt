package com.packager.packager.screens

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.packager.packager.R
import com.packager.packager.base.BaseFragment
import com.packager.packager.databinding.FragmentNavigationBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NavigationFragment :
    BaseFragment<FragmentNavigationBinding>(FragmentNavigationBinding::inflate) {

    private lateinit var navHostFragment: NavHostFragment

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

        init()
    }

    private fun init() {
        initBottomNav()
    }

    private fun initBottomNav() {
        navHostFragment =
            childFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment

        binding.bottomNav.setupWithNavController(navHostFragment.navController)
    }
}