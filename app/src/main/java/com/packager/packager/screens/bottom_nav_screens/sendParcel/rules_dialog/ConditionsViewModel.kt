package com.packager.packager.screens.bottom_nav_screens.sendParcel.rules_dialog

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.packager.packager.models.Condition
import com.packager.packager.repositoy.conditions_repository.ConditionsRepoImpl
import com.packager.packager.retrofit.Handler
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ConditionsViewModel @Inject constructor(
    private val conditionsRepoImpl: ConditionsRepoImpl
): ViewModel() {

    private val mainDispatcher: CoroutineDispatcher = Dispatchers.Main

    private val _conditionsLiveData = MutableLiveData<Handler<Condition>>()
    val conditionsLiveData: LiveData<Handler<Condition>>
        get() = _conditionsLiveData

    fun getConditions() {
        viewModelScope.launch(mainDispatcher) {
            withContext(mainDispatcher) {
               val response = conditionsRepoImpl.getConditions()
                _conditionsLiveData.postValue(response)
            }
        }
    }
}