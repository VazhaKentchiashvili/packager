package com.packager.packager.retrofit

import com.packager.packager.models.Geocoding
import com.packager.packager.utils.Constants.PARCEL_LOCATION_API_ENDPOINT
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ParcelLocationApiService {

    @GET(PARCEL_LOCATION_API_ENDPOINT)
    suspend fun getParcelLocation(
        @Query("latlng")
        latLng: String,
        @Query("key")
        key: String
    ) : Response<Geocoding>

    @GET(PARCEL_LOCATION_API_ENDPOINT)
    suspend fun getServiceCenterLocation(
        @Query("address")
        address: String,
        @Query("key")
        key: String
    ) : Response<Geocoding>
}