package com.packager.packager.retrofit

import com.packager.packager.models.News
import com.packager.packager.models.NewsItems
import com.packager.packager.utils.Constants.NEWS_API_ENDPOINT
import retrofit2.Response
import retrofit2.http.GET

interface NewsApiService {
    @GET(NEWS_API_ENDPOINT)
    suspend fun getNews() : Response<News>
}