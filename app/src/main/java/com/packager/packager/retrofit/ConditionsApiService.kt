package com.packager.packager.retrofit

import com.packager.packager.models.Condition
import com.packager.packager.utils.Constants.CONDITIONS_API_ENDPOINT
import retrofit2.Response
import retrofit2.http.GET

interface ConditionsApiService {

    @GET(CONDITIONS_API_ENDPOINT)
    suspend fun getConditions() : Response<Condition>

}