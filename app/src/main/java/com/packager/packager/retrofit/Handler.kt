package com.packager.packager.retrofit

sealed class Handler<T>(val data:T? = null,val errorMessage:String? = null){
    class Success<T>(data: T):Handler<T>(data)
    class Error<T>(errorMessage: String,data: T? = null):Handler<T>(data,errorMessage)
}
