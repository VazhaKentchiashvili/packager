package com.packager.packager.base

import android.Manifest
import android.app.Dialog
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.packager.packager.R
import com.packager.packager.extensions.startUp

typealias FragmentInflater<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<VB : ViewBinding>(
    private val fragmentInflater: FragmentInflater<VB>,
) : Fragment() {

    private var _binding: VB? = null
    protected val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (_binding == null) {
            _binding = fragmentInflater(inflater, container, false)

            start(layoutInflater, container)
        }

        return binding.root
    }

    protected abstract fun start(inflater: LayoutInflater, container: ViewGroup?)

    fun permissionsRequest(request: ActivityResultLauncher<Array<String>>) {
        request.launch(
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        )
    }

    fun showErrorDialog(title: String, description: String) {

        val dialog = Dialog(requireContext())
        dialog.startUp(R.layout.dialog_layout)
        dialog.findViewById<TextView>(R.id.dialogTitle).text = title
        dialog.findViewById<TextView>(R.id.dialogDescription).text = description

        dialog.findViewById<Button>(R.id.btnCloseDialog).text = "დახურვა"
        dialog.findViewById<Button>(R.id.btnCloseDialog).setOnClickListener {
            dialog.cancel()
        }
        dialog.show()
    }



    fun hasFineLocationPermission() = ActivityCompat.checkSelfPermission(
        requireContext(),
        Manifest.permission.ACCESS_FINE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED

    fun hasCoarseLocationPermission() = ActivityCompat.checkSelfPermission(
        requireContext(),
        Manifest.permission.ACCESS_COARSE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}