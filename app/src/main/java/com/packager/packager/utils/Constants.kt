package com.packager.packager.utils

object Constants {

    const val MAP_API_KEY = "AIzaSyBZry1LMUEWC5rnTfHX8oSMDr07UkB7bvU"

    const val NEWS_BASE_URL = "https://run.mocky.io"
    const val NEWS_API_ENDPOINT = "/v3/6190acac-f23f-4fbf-88bd-45f03be6a4ec"

    const val PARCEL_LOCATION_BASE_URL = "https://maps.googleapis.com"
    const val PARCEL_LOCATION_API_ENDPOINT = "/maps/api/geocode/json"

    const val CONDITIONS_BASE_URL = "https://run.mocky.io"
    const val CONDITIONS_API_ENDPOINT = "/v3/be25ea77-7921-4c17-be2d-2585994f335f"
}