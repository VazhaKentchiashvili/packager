package com.packager.packager.repositoy.logInRepository

import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.packager.packager.DataStoreManager
import com.packager.packager.retrofit.Handler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LogInRepoImplement @Inject constructor(
    private val dataStoreManager: DataStoreManager
) : LogInRepo {
    private val auth = FirebaseAuth.getInstance()

    override suspend fun logIn(email: String, password: String, session: Boolean): Handler<AuthResult> {
        return withContext(Dispatchers.IO) {
            try {
                val result = auth.signInWithEmailAndPassword(email, password).await()
                dataStoreManager.saveSession(session)
                Handler.Success(result)
            } catch (e: Exception) {
                Handler.Error(e.toString())
            }
        }
    }

    override suspend fun logOut(): Handler<Unit> {
        return withContext(Dispatchers.IO) {
            try {
                val result = auth.signOut()
                Handler.Success(result)
            } catch (e: Exception) {
                Handler.Error(e.toString())
            }
        }
    }
}