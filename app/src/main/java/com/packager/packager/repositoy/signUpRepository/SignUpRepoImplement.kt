package com.packager.packager.repositoy.signUpRepository


import android.util.Log.d
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.packager.packager.models.UserModel
import com.packager.packager.retrofit.Handler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class SignUpRepoImplement : SignUpRepo {
    private val auth = FirebaseAuth.getInstance()
    private val firestore = FirebaseFirestore.getInstance()

    override suspend fun register(
        email: String,
        password: String,
        status: String
    ): Handler<AuthResult> {
        return withContext(Dispatchers.IO) {
            try {
                val result = auth.createUserWithEmailAndPassword(email, password).await()
                Handler.Success(result)
            } catch (e: Exception) {
                Handler.Error(e.toString())
            }
        }
    }

    override suspend fun uploadData(userInfo: UserModel): Handler<DocumentReference> {
        return withContext(Dispatchers.IO) {
            try {
                val user: MutableMap<String, String> = mutableMapOf()
                user["name"] = userInfo.name
                user["last_name"] = userInfo.lastName
                user["email"] = userInfo.email
                user["private_number"] = userInfo.privateNumber
                user["mobile_number"] = userInfo.mobileNumber
                d("myUserList", user.toString())

                val result = firestore.collection("users")
                    .add(user)
                    .await()
                Handler.Success(result)
            } catch (e: Exception) {
                Handler.Error(e.toString())
            }
        }
    }

}