package com.packager.packager.repositoy.favorites_repository

import com.google.firebase.firestore.DocumentReference
import com.packager.packager.models.FavoritesModel
import com.packager.packager.retrofit.Handler

interface FavoritesRepo {

    suspend fun uploadFavorites(favoriteInfo: FavoritesModel): Handler<DocumentReference>

    suspend fun getFavorites(): List<FavoritesModel>

    suspend fun setFavoritesData(): List<FavoritesModel>
}