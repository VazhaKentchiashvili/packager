package com.packager.packager.repositoy.signUpRepository


import com.google.firebase.auth.AuthResult
import com.google.firebase.firestore.DocumentReference
import com.packager.packager.models.UserModel
import com.packager.packager.retrofit.Handler

interface SignUpRepo {
    suspend fun register(
        email: String,
        password: String,
        status:String
    ): Handler<AuthResult>


    suspend fun uploadData(userInfo:UserModel):Handler<DocumentReference>


}