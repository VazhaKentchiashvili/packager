package com.packager.packager.repositoy.news_repository

import com.packager.packager.models.News
import com.packager.packager.retrofit.Handler
import com.packager.packager.retrofit.NewsApiService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class NewsRepoImpl @Inject constructor(
    private val newsApiService: NewsApiService
) : NewsRepo {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun getNews(): Handler<News> =
        withContext(ioDispatcher) {
            try {
                val result = newsApiService.getNews()
                if (result.isSuccessful) {
                    Handler.Success(result.body()!!)
                } else {
                    Handler.Error("Request failed")
                }
            } catch (e: Exception) {
                Handler.Error("Request failed")
            }
        }
}