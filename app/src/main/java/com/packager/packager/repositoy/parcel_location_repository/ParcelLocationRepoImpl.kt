package com.packager.packager.repositoy.parcel_location_repository

import com.packager.packager.models.Geocoding
import com.packager.packager.retrofit.Handler
import com.packager.packager.retrofit.ParcelLocationApiService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ParcelLocationRepoImpl @Inject constructor(
    private val parcelLocationApiService: ParcelLocationApiService
) : ParcelLocationRepo {
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun getParcelLocation(
        latLng: String,
        key: String
    ): Handler<Geocoding> =
        withContext(ioDispatcher) {
            try {
                val result = parcelLocationApiService.getParcelLocation(latLng, key)

                if (result.isSuccessful) {
                    Handler.Success(result.body()!!)
                } else {
                    Handler.Error("aee")
                }
            } catch (e: Exception) {
                Handler.Error("failed")
            }
        }

    override suspend fun getServiceCenterLocation(
        address: String,
        key: String
    ): Handler<Geocoding> =
        withContext(ioDispatcher) {
            try {
                val result = parcelLocationApiService.getServiceCenterLocation(address, key)

                if (result.isSuccessful) {
                    Handler.Success(result.body()!!)
                } else {
                    Handler.Error("Request failed")
                }
            } catch (e: Exception) {
                Handler.Error("Request failed")
            }
        }
}