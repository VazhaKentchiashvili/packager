package com.packager.packager.repositoy.manage_password_repository

import com.google.android.gms.tasks.Task
import com.packager.packager.retrofit.Handler

interface ManagePasswordRepo {

    suspend fun changePassword(password:String): Handler<Task<Void>>

}