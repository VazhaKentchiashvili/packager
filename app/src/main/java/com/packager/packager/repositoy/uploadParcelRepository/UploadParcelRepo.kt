package com.packager.packager.repositoy.uploadParcelRepository

import com.google.firebase.firestore.DocumentReference
import com.packager.packager.models.Parcel
import com.packager.packager.retrofit.Handler

interface UploadParcelRepo {

    suspend fun uploadParcel(parcelInfo: Parcel): Handler<DocumentReference>

}