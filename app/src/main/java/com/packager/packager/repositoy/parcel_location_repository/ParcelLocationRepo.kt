package com.packager.packager.repositoy.parcel_location_repository

import com.packager.packager.models.Geocoding
import com.packager.packager.retrofit.Handler

interface ParcelLocationRepo {
    suspend fun getParcelLocation(latLng: String, key: String) : Handler<Geocoding>
    suspend fun getServiceCenterLocation(address: String, key: String) : Handler<Geocoding>
}