package com.packager.packager.repositoy.manage_password_repository

import android.util.Log.d
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.packager.packager.retrofit.Handler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ManagePasswordRepoImplement : ManagePasswordRepo {
    private val auth = FirebaseAuth.getInstance()

    override suspend fun changePassword(password: String): Handler<Task<Void>> {
        return withContext(Dispatchers.IO) {
            try {
                val result = auth.currentUser!!.updatePassword(password)
                Handler.Success(result)
            } catch (e: Exception) {
                Handler.Error(e.toString())
            }
        }
    }
}