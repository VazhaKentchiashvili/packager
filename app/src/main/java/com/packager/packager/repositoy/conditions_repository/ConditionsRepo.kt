package com.packager.packager.repositoy.conditions_repository

import com.packager.packager.models.Condition
import com.packager.packager.retrofit.Handler

interface ConditionsRepo {

    suspend fun getConditions() : Handler<Condition>

}