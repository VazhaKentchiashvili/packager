package com.packager.packager.repositoy.home_parcels_repository

import com.packager.packager.models.Parcel

interface HomeParcelsRepo {

    suspend fun getHomeParcels(): List<Parcel>

}