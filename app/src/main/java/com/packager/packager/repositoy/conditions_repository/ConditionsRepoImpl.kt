package com.packager.packager.repositoy.conditions_repository

import com.packager.packager.models.Condition
import com.packager.packager.retrofit.ConditionsApiService
import com.packager.packager.retrofit.Handler
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

class ConditionsRepoImpl @Inject constructor(
    private val conditionsApiService: ConditionsApiService
) : ConditionsRepo {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun getConditions(): Handler<Condition> =
        withContext(ioDispatcher) {
            try {
                val result = conditionsApiService.getConditions()
                if (result.isSuccessful) {
                    Handler.Success(result.body()!!)
                } else {
                    Handler.Error("Failed")
                }
            }catch (e: Exception) {
                Handler.Error("${e.printStackTrace()}")
            }
        }
}