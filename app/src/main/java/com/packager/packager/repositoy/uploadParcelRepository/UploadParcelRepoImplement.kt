package com.packager.packager.repositoy.uploadParcelRepository

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.packager.packager.models.Parcel
import com.packager.packager.retrofit.Handler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class UploadParcelRepoImplement:UploadParcelRepo {

    private val firestore = FirebaseFirestore.getInstance()


    override suspend fun uploadParcel(parcelInfo: Parcel): Handler<DocumentReference> {
        return withContext(Dispatchers.IO) {
            try {
                val parcel: MutableMap<String, String> = mutableMapOf()
                parcel["parcelSender"] = parcelInfo.parcelSender
                parcel["parcelReceiver"] = parcelInfo.parcelReceiver
                parcel["sendFrom"] = parcelInfo.sendFrom
                parcel["sendWhere"] = parcelInfo.sendWhere
                parcel["email"] = parcelInfo.email


                val result = firestore.collection("sendParcels")
                    .add(parcel)
                    .await()
                Handler.Success(result)
            } catch (e: Exception) {
                Handler.Error(e.toString())
            }
        }
    }
}