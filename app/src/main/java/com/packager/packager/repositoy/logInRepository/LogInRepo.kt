package com.packager.packager.repositoy.logInRepository

import com.google.firebase.auth.AuthResult
import com.packager.packager.retrofit.Handler

interface LogInRepo {
    suspend fun logIn(
        email: String,
        password: String,
        session: Boolean

    ): Handler<AuthResult>

    suspend fun logOut():Handler<Unit>
}