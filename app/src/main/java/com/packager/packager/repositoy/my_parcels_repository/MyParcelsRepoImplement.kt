package com.packager.packager.repositoy.my_parcels_repository

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.packager.packager.models.Parcel
import kotlinx.coroutines.tasks.await

class MyParcelsRepoImplement : MyParcelsRepo {
    private lateinit var firebaseFirestore: FirebaseFirestore
    private lateinit var firebaseAuth: FirebaseAuth
    private val receivedParcels = mutableListOf<Parcel>()

    override suspend fun getMyParcels(): List<Parcel> {
        firebaseFirestore = FirebaseFirestore.getInstance()
        firebaseAuth= FirebaseAuth.getInstance()

        firebaseFirestore.collection("sendParcels")
            .get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    for (document in it.result!!) {
                        if (document.data["parcelReceiver"] == firebaseAuth.currentUser!!.email) {
                            receivedParcels.add(
                                Parcel(
                                    document.data["parcelSender"].toString(),
                                    document.data["parcelReceiver"].toString(),
                                    document.data["sendFrom"].toString(),
                                    document.data["sendWhere"].toString(),
                                    document.data["email"].toString()
                                )
                            )
                        }
                    }
                }
            }.await()
        return receivedParcels
    }


}