package com.packager.packager.repositoy.home_parcels_repository

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.packager.packager.models.Parcel
import kotlinx.coroutines.tasks.await

class HomeParcelsRepoImplement : HomeParcelsRepo {

    private lateinit var firebaseFirestore: FirebaseFirestore
    private lateinit var firebaseAuth: FirebaseAuth
    private val sentParcels = mutableListOf<Parcel>()

    override suspend fun getHomeParcels(): List<Parcel> {
        firebaseFirestore = FirebaseFirestore.getInstance()
        firebaseAuth = FirebaseAuth.getInstance()
        firebaseFirestore.collection("sendParcels")
            .get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    for (document in it.result!!) {
                        if (document.data["parcelSender"] == firebaseAuth.currentUser!!.uid) {
                            sentParcels.add(
                                Parcel(
                                    document.data["parcelSender"].toString(),
                                    document.data["parcelReceiver"].toString(),
                                    document.data["sendFrom"].toString(),
                                    document.data["sendWhere"].toString(),
                                    document.data["email"].toString()
                                )
                            )
                        }
                    }
                }
            }.await()
        return sentParcels
    }
}