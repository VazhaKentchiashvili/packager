package com.packager.packager.repositoy.news_repository

import com.packager.packager.models.News
import com.packager.packager.models.NewsItems
import com.packager.packager.retrofit.Handler

interface NewsRepo {

    suspend fun getNews() : Handler<News>

}