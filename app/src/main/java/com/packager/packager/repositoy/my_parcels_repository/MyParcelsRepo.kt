package com.packager.packager.repositoy.my_parcels_repository

import com.packager.packager.models.Parcel

interface MyParcelsRepo {
    suspend fun getMyParcels(): List<Parcel>
}