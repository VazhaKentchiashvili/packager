package com.packager.packager.repositoy.favorites_repository

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.packager.packager.DataStoreManager
import com.packager.packager.models.FavoritesModel
import com.packager.packager.retrofit.Handler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FavoritesRepoImplement @Inject constructor(
    private val dataStoreManager: DataStoreManager
) : FavoritesRepo {

    private val mAuth = FirebaseAuth.getInstance()
    private val firestore = FirebaseFirestore.getInstance()

    override suspend fun uploadFavorites(favoriteInfo: FavoritesModel): Handler<DocumentReference> {

        return withContext(Dispatchers.IO) {
            try {
                val user: MutableMap<String, String> = mutableMapOf()
                user["name"] = favoriteInfo.name
                user["last_name"] = favoriteInfo.lastName
                user["email"] = favoriteInfo.email
                user["private_number"] = favoriteInfo.privateNumber
                user["mobile_number"] = favoriteInfo.mobileNumber
                user["uid"] = favoriteInfo.uid

                val result = firestore.collection("favorites")
                    .add(user)
                    .await()
                Handler.Success(result)
            } catch (e: Exception) {
                Handler.Error(e.toString())
            }
        }
    }

    private val favoritesList = mutableListOf<FavoritesModel>()

    override suspend fun getFavorites(): List<FavoritesModel> {
        firestore.collection("favorites")
            .get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    for (document in it.result!!) {
                        if (document.data["uid"] == mAuth.currentUser!!.uid) {
                            favoritesList.add(
                                FavoritesModel(
                                    document.data["name"].toString(),
                                    document.data["last_name"].toString(),
                                    document.data["email"].toString(),
                                    document.data["private_number"].toString(),
                                    document.data["mobile_number"].toString(),
                                    document.data["uid"].toString()
                                )
                            )
                        }
                    }
                }
            }.await()
        return favoritesList
    }

    override suspend fun setFavoritesData(): List<FavoritesModel> {
        firestore.collection("favorites")
            .get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    CoroutineScope(Dispatchers.IO).launch {
                        for (document in it.result!!) {
                            if (document.data["mobile_number"] == dataStoreManager.readPersonPhoneNumber()) {
                                favoritesList.add(
                                    FavoritesModel(
                                        document.data["name"].toString(),
                                        document.data["last_name"].toString(),
                                        document.data["email"].toString(),
                                        document.data["private_number"].toString(),
                                        document.data["mobile_number"].toString(),
                                        document.data["uid"].toString()
                                    )
                                )
                            }
                        }
                    }
                }
            }.await()
        return favoritesList
    }
}